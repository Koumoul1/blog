from django.shortcuts   import render
from django.http        import HttpResponse
import os
import markdown

# Create your views here.

global thisdir
thisdir = os.path.dirname(__file__)

def homepage(request):
    files   = os.listdir(thisdir + "/../static/articles")
    titles  = list(map((lambda name: name[:name.find('.')]), files))
    return render(request, "homepage/homepage.html", locals())

def article(request, title):
    mdcontent   = open(thisdir + "/../static/articles/" + title + ".md", 'r').read()
    content     = markdown.markdown(mdcontent)
    return render(request, "homepage/article.html", locals())

def list_articles(request, year, month=1):
    return HttpResponse(
        "Vous avez demandé les articles de {0} {1}.".format(year, month))