from django.urls    import path
from .              import views


urlpatterns = [
    path('', views.homepage, name="homepage"),
    path('article/<str:title>', views.article, name="article"),
    path('articles/<int:year>/', views.list_articles, name="articles"),
    path('articles/<int:year>/<int:month>', views.list_articles, name="articles"),
]
